const fs = require('fs');
const http = require('http');
const https = require('https');
const express = require('express');
const compression = require('compression');

const app = express();
app.use(compression());

// Certificate

const privateKey = fs.readFileSync('/etc/letsencrypt/live/recruitment.sportsology.pro/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/recruitment.sportsology.pro/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/recruitment.sportsology.pro/chain.pem', 'utf8');
app.use(express.static(__dirname + "/static" ));
const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};

app.use((req, res) => {
	res.sendFile(__dirname + '/static/index.html');
});

// Create and start https servers

const httpsServer = https.createServer(credentials, app);


httpsServer.listen(443, () => {
	console.log('HTTPS Server running on port 443');
});
